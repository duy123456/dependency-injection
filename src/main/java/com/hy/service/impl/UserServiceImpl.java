package com.hy.service.impl;

import com.hy.dao.IUserDao;
import com.hy.service.IUserService;

/**
 * Created With IntelliJ IDEA.
 * Descriptions:
 * author: Mr.Du
 * Date: 2022/8/26
 * Time: 13:48
 */
public class UserServiceImpl implements IUserService {
    
    private IUserDao userDao;
    
//    public UserServiceImpl(IUserDao userDao) {
//        this.userDao = userDao;
//    }
    
    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }
    
    @Override
    public void save() {
        userDao.save();
    }
}
